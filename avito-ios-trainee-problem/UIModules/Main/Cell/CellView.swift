import UIKit

final class CellView: UICollectionViewCell {
    static let identifier = "CellView"
    private enum Constants {
        static let contentViewCornerRadius: CGFloat = 4.0

        static let verticalSpacing: CGFloat = 8.0
        static let horizontalPadding: CGFloat = 16.0
        static let profileDescriptionVerticalPadding: CGFloat = 8.0
    }
    

    let nameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica Neue", size: 30)
        return label
    }()

    let phoneNumberLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        return label
    }()

    let skillsLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
        setupLayouts()
    }

    private func setupViews() {
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = Constants.contentViewCornerRadius

        contentView.addSubview(nameLabel)
        contentView.addSubview(phoneNumberLabel)
        contentView.addSubview(skillsLabel)
    }

    private func setupLayouts() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        skillsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 5),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 5),
            nameLabel.trailingAnchor.constraint(equalTo: phoneNumberLabel.leadingAnchor ,constant: -5),
            nameLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40)
        ])
        
        NSLayoutConstraint.activate([
            skillsLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            skillsLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            skillsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 5),
            skillsLabel.trailingAnchor.constraint(equalTo: nameLabel.trailingAnchor),
            skillsLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            phoneNumberLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5),
            phoneNumberLabel.topAnchor.constraint(equalTo: nameLabel.topAnchor),
            phoneNumberLabel.bottomAnchor.constraint(equalTo: skillsLabel.bottomAnchor),
            phoneNumberLabel.widthAnchor.constraint(equalToConstant: 100)
        ])
//        didSetupConstraints = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup(with employee: Employee) {
        nameLabel.text = employee.name
        phoneNumberLabel.text = employee.phoneNumber
        skillsLabel.text = employee.skills.joined(separator: " ,")
    }
}

