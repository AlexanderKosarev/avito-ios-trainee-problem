import UIKit

class ViewController: UIViewController {
    private var viewModel: MainViewModel!
    let collectionView = MainView()
    private var activityIndicator = UIActivityIndicatorView(style: .large)
    
    private enum LayoutConstant {
        static let spacing: CGFloat = 8.0
        static let itemHeight: CGFloat = 100.0
    }
    
    override func viewDidLoad() {
        viewModel = MainViewModel()
        checkConnection()
        super.viewDidLoad()
        setupView()
        setActivityIndicator()
        setUpData()
        NotificationCenter.default.addObserver(self, selector: #selector(showOfflineDeviceUI(notification:)), name: NSNotification.Name.connectivityStatus, object: nil)
    }
    
    private func setUpData() {
        viewModel.getList {
            self.collectionView.reloadData()
            self.activityIndicator.stopAnimating()
        }
        self.collectionView.refreshControler.endRefreshing()
        
    }
    
    private func setupView() {
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
    }
    
    private func checkConnection() {
        print("Check connection")
        if NetworkMonitor.shared.isConnected {
            print("connected")
            return
        } else {
            networkAllert()
        }
    }
    
    private func networkAllert() {
        let networkAlert = UIAlertController(title: "No connection", message: "looks like you have no internet :c", preferredStyle: .alert)
        networkAlert.addAction(UIAlertAction(title: "Try again", style: .default, handler: { [weak self] action in
            self?.viewDidLoad()
        }))
        networkAlert.addAction(UIAlertAction(title: "Show last data", style: .default, handler: { action in
            self.collectionView.refreshControler.endRefreshing()
            self.viewModel.getList {
                self.collectionView.reloadData()
            }
        }))
        present(networkAlert, animated: true)
    }
    private func setActivityIndicator() {
        self.view.addSubview(activityIndicator)
        activityIndicator.layer.zPosition = 1
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = .red
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        activityIndicator.startAnimating()
    }
    @objc func showOfflineDeviceUI(notification: Notification) {
            if NetworkMonitor.shared.isConnected {
                print("Connected")
            } else {
                DispatchQueue.main.async {
                    self.networkAllert()
                    print("Not connected")
                }
                
                
            }
        }
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.employees.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellView.identifier, for: indexPath) as! CellView

        let employe = viewModel.employees[indexPath.row]
        cell.setup(with: employe)
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = itemWidth(for: view.frame.width, spacing: LayoutConstant.spacing)

        return CGSize(width: width, height: LayoutConstant.itemHeight)
    }

    func itemWidth(for width: CGFloat, spacing: CGFloat) -> CGFloat {
        let itemsInRow: CGFloat = preferredInterfaceOrientationForPresentation.isLandscape ? 2 : 1

        let totalSpacing: CGFloat = 2 * spacing + (itemsInRow - 1) * spacing
        let finalWidth = (width - totalSpacing) / itemsInRow

        return floor(finalWidth)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: LayoutConstant.spacing, left: LayoutConstant.spacing, bottom: LayoutConstant.spacing, right: LayoutConstant.spacing)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return LayoutConstant.spacing
    }
}
