import UIKit

final class MainView: UICollectionView {
    let refreshControler = UIRefreshControl()
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let viewLayout = UICollectionViewFlowLayout()
        super.init(frame: .zero, collectionViewLayout: viewLayout)
        
        self.register(CellView.self, forCellWithReuseIdentifier: CellView.identifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setRefreshControler() {
            refreshControler.attributedTitle = NSAttributedString(string: "Pull to refresh")
            self.addSubview(refreshControler)
            refreshControler.layer.zPosition = -1
        }
}
