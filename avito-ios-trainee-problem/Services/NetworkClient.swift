import Foundation

fileprivate let LAST_UPDATE_KEY = "lastUpdate"
fileprivate let CACHED_DATA_KEY = "cachedData"
fileprivate let ONE_HOUR: Double = 60 * 60
fileprivate let MOCK_URL = URL(string: "https://run.mocky.io/v3/1d1cb4ec-73db-4762-8c4b-0b8aa3cecd4c")

final class NetworkClient {
    private var dateFormatter = ISO8601DateFormatter()
    
    private var lastTimeOfUpdate = UserDefaults.standard.string(forKey: LAST_UPDATE_KEY) ?? ""
    
    private var needToUpdate: Bool {
        (
            dateFormatter.date(from: lastTimeOfUpdate) ?? Date() - ONE_HOUR
        ).timeIntervalSinceNow <= -ONE_HOUR && NetworkMonitor.shared.isConnected
    }
    
    private func fetchUserDefaultsData<T: Codable>(key: String, onComplete: @escaping (_ model: T) -> Void) {
        let decoder = JSONDecoder()
        let data = UserDefaults.standard.data(forKey: key)
        do {
            let result = try decoder.decode(T.self, from: data!)
            onComplete(result)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func fetchNetworkData<T: Codable>(onComplete: @escaping (_ model: T) -> Void) {
        let urlSession = URLSession(configuration: .default)
        let decoder = JSONDecoder()
        
        let task = urlSession.dataTask(with: MOCK_URL!) { data, response, error in
            guard let data = data,
                  error == nil,
                  let _ = response as? HTTPURLResponse else {
                print(error.debugDescription)
                return
            }
            
            do {
                let result = try decoder.decode(T.self, from: data)
                let lastUpdate = self.dateFormatter.string(from: Date())
                UserDefaults.standard.setValue(data, forKey: CACHED_DATA_KEY)
                UserDefaults.standard.setValue(lastUpdate, forKey: LAST_UPDATE_KEY)
                self.lastTimeOfUpdate = lastUpdate
                onComplete(result)
            } catch {
                print(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    func getData<T: Codable>(onComplete: @escaping (_ model: T) -> Void) {
        if needToUpdate {
            fetchNetworkData() { (model: T) in
                onComplete(model)
            }
        } else {
            fetchUserDefaultsData(key: CACHED_DATA_KEY) { (model: T) in
                onComplete(model)
            }
        }
    }
}
