import Foundation

final class MainViewModel {
    private let networkClient = NetworkClient()
    var employees: [Employee] = []
    func getList(onComplete: @escaping () -> Void) {
        networkClient.getData() {[weak self] (model: Employees) in
            self?.employees = model.company.employees.sorted { $0.name < $1.name}
            DispatchQueue.main.async {
                onComplete()
            }
        }
    }
}
